with sales_with_day as (
    select
        time_id::date as date,
        extract(year from time_id::date) as year,
        extract(week from time_id::date) as week,
        to_char(time_id::date, 'day') as day_of_week,
        sum(amount_sold) as total_amount_sold,
        sum(sum(amount_sold)) over (partition by extract(year from time_id::date), extract(week from time_id::date) order by time_id::date) as cum_sum,
        case
            when to_char(time_id::date, 'day') = 'monday' then
                round(avg(sum(amount_sold)) over (order by time_id::date rows between 2 preceding and 1 following), 2)
            when to_char(time_id::date, 'day') = 'friday' then
                round(avg(sum(amount_sold)) over (order by time_id::date rows between 1 preceding and 2 following), 2)
            else
                round(avg(sum(amount_sold)) over (order by time_id::date rows between 1 preceding and 1 following), 2)
        end as centered_3_day_avg
    from sh.sales s
    where extract(year from time_id::date) = 1999
      and extract(week from time_id::date) in (49, 50, 51)
    group by date, year, week, day_of_week
)
select * from sales_with_day order by date;

-------------------------1
-- task:
-- calculate the centered 3-day average of total sales for each day of the week in the specified weeks of the year 1999.
-- the centered 3-day average is calculated differently for mondays, fridays, and other days of the week.

-- sales data with day-wise aggregation for the specified weeks in 1999
with sales_with_day as (
    select
        time_id::date as date,
        extract(year from time_id::date) as year,
        extract(week from time_id::date) as week,
        to_char(time_id::date, 'day') as day_of_week,
        sum(amount_sold) as total_amount_sold,
        sum(sum(amount_sold)) over (partition by extract(year from time_id::date), extract(week from time_id::date) order by time_id::date) as cum_sum,
        case
            when to_char(time_id::date, 'day') = 'monday' then
                round(avg(sum(amount_sold)) over (order by time_id::date rows between 2 preceding and 1 following), 2)
            when to_char(time_id::date, 'day') = 'friday' then
                round(avg(sum(amount_sold)) over (order by time_id::date rows between 1 preceding and 2 following), 2)
            else
                round(avg(sum(amount_sold)) over (order by time_id::date rows between 1 preceding and 1 following), 2)
            end as centered_3_day_avg
    from sh.sales s
    where extract(year from time_id::date) = 1999
      and extract(week from time_id::date) in (49, 50, 51)
    group by date, year, week, day_of_week
)

-- select and display the results
select * from sales_with_day order by date;

-- sales_with_day cte:
-- this cte calculates the total amount sold for each day of the specified weeks in 1999.
-- the select statement extracts the date, year, week, and day of the week from the time_id column.
-- the sum function is used to calculate the total amount sold for each day.
-- the sum(sum(amount_sold)) over (...) calculates the cumulative sum of total amount sold for each week.
-- the case statement determines the calculation of the centered 3-day average based on the day of the week.
-- the result is stored in the temporary table named sales_with_day.

-- main query:
-- this query selects and displays the results from the sales_with_day cte.
-- it includes columns for date, year, week, day of the week, total amount sold, cumulative sum, and centered 3-day average.
-- the result provides insights into the total sales and centered 3-day average for each day of the specified weeks in 1999.
